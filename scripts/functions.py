######---------Importing modules 
import pyodbc
import datetime
import logging
import pandas as pd
import numpy as np
from jinjasql import JinjaSql
import os
import sys
from scipy.special import boxcox1p, inv_boxcox1p
import smtplib

#get system path and system date
cwd = os.path.abspath(os.path.join(sys.path[0], os.pardir)) 
date_time = datetime.datetime.now()

######---------Setting the log file
logfile_name = os.path.join(cwd, "logfile.log")
logging.basicConfig(filename=logfile_name, level=logging.INFO)

######---------Defining functions
from send_email import send_email

#connecting to database function
def connect_to_database(server, database, username, password, port, database_name, retries=3):

    tries = 0
    cnxn = None
    
    while (tries < retries) :

        #STEP 1: try to connect to database & server
        try:
            # Connection string
            #cnxn = pyodbc.connect('DRIVER={FreeTDS};SERVER='+server+';DATABASE='+database+';UID='+username+';PWD='+ password)
            cnxn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server};SERVER='+server+';DATABASE='+database+';UID='+username+';PWD='+ password)
            logging.info(f"{str(date_time)}: {str(database_name)}:  Database connexion success")            
            break
            
        except Exception as ex: 

            logging.error(f"{str(date_time)}: {str(database_name)}:  Database connexion failure")
            
            template = "An exception of type {0} occurred. Arguments:\n{1!r}"
            
            logging.warning(template.format(type(ex).__name__, ex.args))
            cnxn = None
            tries+=1
            
    return cnxn

def get_current_campagne(db_cnxn,default_campagne, idsociete):

    #logging
    logging.info(str(date_time) + " Automatic generation, getting current campain")

    try :

        #getting default campain filter if automatic generate
        date_time_str = "'" + str(datetime.datetime.today().strftime('%Y-%d-%m'))+"'"
        query = f"Select Code_compagne from compagne where Date_debut <= {date_time_str} and Date_Fin >= {date_time_str} and idsociete = {idsociete}"
        campagne_df = pd.read_sql_query(query, db_cnxn)
                
        #getting the campain
        campagne= campagne_df['code_compagne'][0]
        logging.info(str(date_time) + " Got current campain " + str(campagne))
        
    except Exception as ex: 

        #getting default campain if could not get current campain
        campagne = default_campagne
        logging.info(str(date_time) + " Couldn't get current campain, got default campain " + str(campagne))

        #logging
        template = "An exception of type {0} occurred. Arguments:\n{1!r}"
        logging.warning(str(date_time) + template.format(type(ex).__name__, ex.args))

    return campagne

#function that turns a python list to an applicable parameter for the in statement (parenthese are not included)
def turnlist_to_in_statement(list_to_turn):
    
    string = list_to_turn[0]
    for element in list_to_turn[1:]:
        string = f'{string},{element}' 
    
    return string

#Function to get start and end date
def get_periode(db_cnxn, automatic_generation, idsociete, sent_campagne=None):
    
    logging.info(str(date_time) + " -------------------- Getting start and end date --------------------")
    print(str(date_time) + " -------------------- Getting start and end date --------------------")

    try :
        
        if automatic_generation is True:
            
            ##logging
            logging.info(f"{str(date_time)}: Automatic generation getting current campain")

            ##getting current campain filter if automatic generation
            #query to get start and end date
            date_time_str = "'" + str(datetime.datetime.today().strftime('%Y-%d-%m'))+"'"
            query = f"SELECT Date_debut, Date_fin FROM Fermes_compagne WHERE ID_compagne = (SELECT ID_compagne FROM Fermes_compagne where Date_debut <=" + f"{date_time_str} and Date_Fin >=" + f"{date_time_str} and IDsociete in ({turnlist_to_in_statement(idsociete)}))"
            
            #executing the query and assigning to date_debut and date_fin variables
            df = pd.read_sql_query(query, db_cnxn)
            date_debut = f"'{df['Date_debut'][0].strftime('%Y-%m-%d')}'"
            date_fin =  f"'{df['Date_fin'][0].strftime('%Y-%m-%d')}'" 
            
            #getting code campagne
            query2 = "SELECT Code_compagne FROM Fermes_compagne where Date_debut <=" + f"{date_time_str} and Date_Fin >=" + f"{date_time_str} and IDsociete in ({turnlist_to_in_statement(idsociete)})"
            df2 = pd.read_sql_query(query2, db_cnxn)
            code_campagne = df2['Code_compagne'][0]
            
            ##logging
            logging.info(str(date_time) + " Successfully loaded period between " + str(date_debut) + " and " + str(date_fin))
        
        else:
            
            ##logging
            logging.info(str(date_time) + " Manual generation, getting campain chosen by the user " + str(sent_campagne))

            
            ##getting current campain filter if manual generation
            #query to get start and end date
            query = f"SELECT Date_debut, Date_fin FROM Fermes_compagne WHERE ID_compagne = (SELECT ID_compagne FROM Fermes_compagne where ID_compagne = {sent_campagne})"
            
            #executing the query and assigning to date_debut and date_fin variables
            df = pd.read_sql_query(query, db_cnxn)
            date_debut = f"'{df['Date_debut'][0].strftime('%Y-%m-%d')}'"
            date_fin =  f"'{df['Date_fin'][0].strftime('%Y-%m-%d')}'"
            
            #getting code campagne
            query2 = f"SELECT Code_compagne FROM Fermes_compagne where ID_compagne = {sent_campagne}"
            df2 = pd.read_sql_query(query2, db_cnxn)
            code_campagne = df2['Code_compagne'][0]

            ##logging
            logging.info(str(date_time) + " Successfully loaded period between " + str(date_debut) + " and " + str(date_fin))
            
    except Exception as ex: 
        
        logging.error(str(date_time) + ': Error getting the prediction start and end date')
        template = "An exception of type {0} occurred. Arguments:\n{1!r}"
        logging.warning(template.format(type(ex).__name__, ex.args))
        date_debut, date_fin = None, None

    return date_debut, date_fin, code_campagne

#querying data function
def query_data(db_cnxn, query_file, params, query_name=''):
    
    #logging
    logging.info(str(date_time) + " --------------------------- Querying data ---------------------------")

    try:

        #readying query 
        params = params
        j = JinjaSql(param_style='pyformat')
        query, bind_params = j.prepare_query(query_file, params)

        #executing query
        df = pd.read_sql_query(query % bind_params, db_cnxn)

        #logging
        logging.info(str(date_time) + " Imported " + str(df.shape[0])+ " " + str(query_name) + " lines succesfully")

    except Exception as ex:

        #logging
        logging.error(str(date_time) + ': problem with the query')
        template = "An exception of type {0} occurred. Arguments:\n{1!r}"
        logging.warning(str(date_time) + template.format(type(ex).__name__, ex.args))
        df = None

    return df

#preparing data function
def prepare_data(df, columns_to_str, columns_to_agg,delete_missing, is_boxcox, columns_to_trans, lam=0.25):
    
    #logging
    logging.info(str(date_time) + ' --------------------------- Preparing data ---------------------------')

    try: 
        #getting intial shape of dataframe
        init_shape = df.shape
        
        #delete missing / null / NaN data
        if delete_missing==True:
            
            #getting number of missing data
            missing_data = df.isnull().sum().sum()
            logging.info(str(date_time) + ' Detected ' + str(missing_data) + ' missing data, proceeding for deletion')
            
            #deleting missing data
            df=df[df.isnull() == False]
            df=df[df['row'].isna()==False]
            
        #transforming ids to int then string
        for column in columns_to_str:
            try:
                df[column]=df[column].astype(int)
                df[column]=df[column].astype(str)
            except:
                df[column]=df[column].astype(str)

        #initial / final shape
        final_shape = df.shape
        logging.info(str(date_time) + ' Initial shape : ' + str(init_shape) + ' / Final shape after deletion : ' + str(final_shape))
        
        #grouping by row
        df_grouped=df.groupby(columns_to_agg).sum()
        final_shape_dfgrouped = df_grouped.shape
        logging.info(str(date_time) + ' Grouped by row, new shape : '+str(final_shape_dfgrouped))

        #boxcox transformation
        if is_boxcox==True:
            for column in columns_to_trans:
                df_grouped[column]=boxcox1p(df_grouped[column],lam)
        logging.info(str(date_time) + ' Transformed ' + str(columns_to_trans) + ' with boxcox function')
            
    except Exception as ex: 

        #logging
        logging.error(str(date_time) + ': problem with the data preparation')
        template = "An exception of type {0} occurred. Arguments:\n{1!r}"
        logging.warning(str(date_time) + template.format(type(ex).__name__, ex.args))
        df_grouped = None
    
    return df_grouped,df

#making a single prediction using model
def predict_single(model,predictor):

    input=np.array([predictor]).astype(np.float64)
    prediction = model.predict(input) 

    return prediction

#making a prediction on dataframe
def predict_df(df, model,model_name,features,is_boxcox=True, lam=0.25):
    
    #logging
    logging.info(str(date_time) + ' --------------------------- Predicting data ---------------------------')

    try:
        #logging
        logging.info(str(date_time) + ' Predicting using '+ str(model_name))

        list_prediction = list()

        #looping on rows to append the dataframe with the predictions
        for index,row in df.iterrows():
            prediction = predict_single(model,[row[f] for f in features])
            list_prediction.append(prediction[0])
        
        #creating new column for predicted yield
        df['predicted_yield'] = list_prediction
        
        #if boxcox transform all the df
        if is_boxcox:
            df = inv_boxcox1p(df, lam)

        logging.info(str(date_time) + ' Predicted ' + str(df.shape[0]) + ' lines succesfully')

    except Exception as ex: 

        #logging
        logging.error(str(date_time) + ': problem with the prediction')
        template = "An exception of type {0} occurred. Arguments:\n{1!r}"
        logging.warning(str(date_time) + template.format(type(ex).__name__, ex.args))
        df = None


    return df

#function to get yield per tree based on spathes
def divide_yield_ontrees(df_harvest, df_counted, df_remaining, df_counted_predicted, df_remaining_predicted):
    
    #logging
    logging.info(str(date_time) + ' --------------------------- Dividing yield on trees ---------------------------')
    
    try:
    
        #droping indexes
        df_counted_predicted.reset_index(inplace=True)
        df_remaining_predicted.reset_index(inplace=True)
        
        #transforming row and plot_id columns to str for predicted and initial dataframes
        columns_to_str = ['row','plot_id']
        for df in [df_counted_predicted,df_remaining_predicted,df_counted,df_remaining]:
            for column in columns_to_str:
                try:
                    df[column]=df[column].astype(int)
                    df[column]=df[column].astype(str)
                except:
                    df[column]=df[column].astype(str)
        
        #transforming tree_id column for initial dataframes 
        columns_to_str = ['tree_id']
        for df in [df_counted,df_remaining,df_harvest]:
            try:
                df[columns_to_str]=df[columns_to_str].astype(int)
                df[columns_to_str]=df[columns_to_str].astype(str)
            except:
                df[columns_to_str]=df[columns_to_str].astype(str)      
                
        #renaming columns before joins
        df_counted_predicted.rename(columns={'predicted_yield':'predicted_yield_counted'},inplace=True)
        df_remaining_predicted.rename(columns={'predicted_yield':'predicted_yield_remaining'},inplace=True)
        df_counted.rename(columns={'counted_branches':'counted_branches_ind'},inplace=True)
        df_remaining.rename(columns={'remaining_branches':'remaining_branches_ind'},inplace=True)
        
        df_counted.rename(columns={'plot_id':'plot_id_origin'},inplace=True)  
        df_remaining.rename(columns={'plot_id':'plot_id_origin'},inplace=True)
        df_counted.rename(columns={'row':'row_origin'},inplace=True)  
        df_remaining.rename(columns={'row':'row_origin'},inplace=True)  
        
        
        #merging between the dataframes            
        if df_counted.shape[0] > df_remaining.shape[0]:
            result = pd.merge(df_counted, df_remaining,how='left',left_on=['tree_id'], right_on=['tree_id'])
        else:
            result = pd.merge(df_remaining, df_counted,how='left',left_on=['tree_id'], right_on=['tree_id'])
        
        result = pd.merge(result, df_harvest,how='left',left_on=['tree_id'], right_on=['tree_id'])

        result = pd.merge(result, df_counted_predicted,how='left',left_on=['plot_id_origin_x','row_origin_x'], right_on=['plot_id','row'])

        result = pd.merge(result, df_remaining_predicted,how='left',left_on=['plot_id_origin_x','row_origin_x'], right_on=['plot_id','row'])


        #calculating the weights of each tree (percentage of its spathes in the whole row)
        result['weight_counted_branches'] = result['counted_branches_ind'] / result['counted_branches']
        result['weight_remaining_branches'] = result['remaining_branches_ind'] / result['remaining_branches']
        
        #calculating the individual yield
        result['yield_remaining'] = result['predicted_yield_remaining'] * result['weight_remaining_branches']
        result['yield_counted'] = result['predicted_yield_counted'] * result['weight_counted_branches']

        #preparing the final dataframe
        result = result[['id_societe_x','societe_x','id_ferme_x','ferme_x','plot_id_origin_x','parcelleculturale_x','row_origin_x','tree_id',
                 'counted_branches_ind','remaining_branches_ind',
                 'yield','yield_counted','yield_remaining','Code_Arbre_x']]
        
        result.fillna(0,inplace=True)
        #logging
        logging.info(str(date_time) + ' Final dataframe prepared successfully')
        
    except Exception as ex: 

        #logging
        logging.error(str(date_time) + ': Problem with the division of yield on trees')
        template = "An exception of type {0} occurred. Arguments:\n{1!r}"
        logging.warning(str(date_time) + template.format(type(ex).__name__, ex.args))
        result = None
    
    return result

#inserting metadata in generation table
def insert_metadata(db_cnxn,generated_by,spathes_id,harvest_id,campagne):
   
    #logging
    logging.info(str(date_time) + ' --------------------------- Uploading metadata ---------------------------')
   
    try : 

        #inserting data into generation table
        values_statement = f"(CURRENT_TIMESTAMP,'{generated_by}',{spathes_id},{harvest_id},'{campagne}')"
        metadata_query = f"INSERT INTO generation(date_generated, generated_by,spathes_id,harvest_id,campagne) VALUES {values_statement}"
        cursor = db_cnxn.cursor()
        cursor.execute(metadata_query)
        print(metadata_query)
        db_cnxn.commit()
        cursor.close()
        
        #logging
        logging.info(str(date_time) + ' Uploaded metatadata successfully')
        
        #getting id of the insert record
        id_query = "SELECT top 1 id from generation ORDER BY date_generated desc"
        df = pd.read_sql_query(id_query, db_cnxn)
        id = df['id'][0]
        
    except Exception as ex :
        
        #logging
        logging.error(str(date_time) + ': Problem with inserting metadata')
        template = "An exception of type {0} occurred. Arguments:\n{1!r}"
        logging.warning(str(date_time) + template.format(type(ex).__name__, ex.args))
        id = None
    
    return id

#inserting predicition details in details_prediction table
def insert_details(db_cnxn,id_generation,code_compagne, df):
    
    #logging
    logging.info(str(date_time) + ' --------------------------- Uploading details data ---------------------------')
   
    try :
        
        #setting number of rows to insert to control at the end of the generation
        cursor = db_cnxn.cursor()
        all_values_statements = list()
        #l = [x for x in range(i,j)]
        #df = df.iloc[l].copy()
        
        #looping on rows to generate list parameter
        for _, row in df.iterrows():
            
            #setting variables
            id_societe = int(row['id_societe_x'])
            societe = str(row['societe_x'])
            campagne = str(code_compagne)
            id_ferme = int(row['id_ferme_x'])
            ferme = str(row['ferme_x'])
            id_parcelleculturale= int(row['plot_id_origin_x'])
            parcelleculturale = str(row['parcelleculturale_x'])
            ligne = str(row['row_origin_x'])
            id_arbre= int(row['tree_id'])
            arbre = str(row['Code_Arbre_x'])
            spathes_comptes = int(row['counted_branches_ind'])
            spathes_conserves= int(row['remaining_branches_ind'])
            rdt_predit_comptes = float(round(row['yield_counted'],8))
            rdt_predit_conserves = float(round(row['yield_remaining'],8))
            rdt_reel = float(round(row['yield'],8))
            id_generation_insert = int(id_generation)
            
            #constructing list for query
            values_statement = (id_societe,societe,campagne,id_ferme,ferme,id_parcelleculturale,
                                parcelleculturale,ligne,id_arbre,arbre,spathes_comptes,spathes_conserves,
                                rdt_predit_comptes,rdt_predit_conserves,rdt_reel,id_generation_insert)
            all_values_statements.append(values_statement)
            
        #constructing query
        details_query = f"INSERT INTO detail_predictions(id_societe, societe, campagne, id_ferme, ferme, id_parcelleculturale, parcelleculturale,ligne, id_arbre, arbre, spathes_comptes, spathes_conserves, rdt_predit_comptes, rdt_predit_conserves,rdt_reel, id_generation) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)"
           
        #inserting data
        cursor.fast_executemany = True
        cursor.executemany(details_query, all_values_statements)
        db_cnxn.commit()
        
        #updating state and message in generation table        
        message = f"All {df.shape[0]} rows succesfully inserted"
        state = "Success" 
        cursor.execute(f"UPDATE generation set state = '{state}' WHERE id = {id_generation}")
        cursor.execute(f"UPDATE generation set message = '{message}' WHERE id = {id_generation}")
        db_cnxn.commit()
        cursor.close()
        
        logging.info(str(date_time)+ ": " + message)
        
            
    except Exception as ex :
        
        #logging
        logging.error(str(date_time) + ': Problem with inserting details data')
        template = "An exception of type {0} occurred. Arguments:\n{1!r}"
        logging.warning(str(date_time) + template.format(type(ex).__name__, ex.args))
        message = "Problem with inserting details data"
        state = "Failure"
        
        #updating state and message in generation table 
        cursor = db_cnxn.cursor()
        cursor.execute(f"UPDATE generation set state = '{state}' WHERE id = {id_generation}")
        cursor.execute(f"UPDATE generation set message = '{message}' WHERE id = {id_generation}")
        db_cnxn.commit()
        cursor.close()

    return state, message

#main function for prediction, uses all the functions above
def prediction_pipeline(pipeline_params):

    try :
    
        logging.info(str(date_time) + " -------------------------------- Starting prediction pipeline --------------------------------")
        print(str(date_time) + " -------------------------------- Starting prediction pipeline --------------------------------")

        ######---------Step 1: connecting to database
        #getting database identificators
        production_database = pipeline_params['production_database']
        warehouse_database = pipeline_params['warehouse_database']

        #Setting up production database
        prod_server = production_database['server']
        prod_database = production_database['database']
        prod_username = production_database['username']
        prod_password = production_database['password']
        prod_port = production_database['port']
        prod_name = production_database['name']

        #Setting up datawarehouse database
        dw_server = warehouse_database['server']
        dw_database = warehouse_database['database']
        dw_username = warehouse_database['username']
        dw_password = warehouse_database['password']
        dw_port = warehouse_database['port']
        dw_name = warehouse_database['name']

        #Using function to connect to production and warehouse databases
        prod_cnxn = connect_to_database(prod_server,prod_database,prod_username,prod_password,prod_port,prod_name)
        dw_cnxn = connect_to_database(dw_server,dw_database,dw_username,dw_password,dw_port,dw_name)

        #Controlling whether the database connexion has been made succesfully
        if (prod_cnxn is None) or (dw_cnxn is None):
            
            #logging
            logging.critical(str(date_time) + " Couldn't establish connexion to database")
            print(str(date_time) + " Couldn't establish connexion to database")
            state = "Failure"
            message = "Couldn't establish connexion to database"

        else: 

            #setting parameters
            automatic_generation = pipeline_params['automatic_generation']

            ######---------Step 2: Controlling if the generation is automatic or manual
            if automatic_generation: 

                #get list of societe ids
                query = 'SELECT ID FROM SOCIETE'
                idssocietes = pd.read_sql_query(query, prod_cnxn)
                idssocietes= idssocietes['ID'].tolist()

                #get list of ferme ids
                query = f'SELECT IDFermes FROM fermes WHERE ID_societe in ({turnlist_to_in_statement(idssocietes)})'
                idsfermes = pd.read_sql_query(query, prod_cnxn)
                idsfermes= idsfermes['IDFermes'].tolist()
                
                #get sent campagne
                sent_campagne = pipeline_params['sent_campagne']
            
            else:
                
                #get all parameters
                idssocietes = [pipeline_params['idsociete']]
                idsfermes = [pipeline_params['idferme']]
                sent_campagne = pipeline_params['sent_campagne']
        
            #logging
            logging.info(str(date_time) + " -------------------- Setting pipeline parameters --------------------")
            print(str(date_time) + " -------------------- Setting pipeline parameters --------------------")
                
            #logging
            logging.info(f" {str(date_time)}: Pipeline generated automatically: {automatic_generation} ")
            logging.info(f" {str(date_time)}: Companies IDs used for generation: {idssocietes} ")
            logging.info(f" {str(date_time)}: Farms IDs used for generation: {idsfermes} ")
            
            print(f" {str(date_time)}: Pipeline generated automatically: {automatic_generation} ")
            print(f" {str(date_time)}: Companies IDs used for generation: {idssocietes} ")
            print(f" {str(date_time)}: Farms IDs used for generation: {idsfermes} ")
            
            ######---------Step 3: Getting generation Start and End date 

            #call function to get start and end date
            date_debut, date_fin, code_compagne= get_periode(prod_cnxn, automatic_generation, idssocietes,sent_campagne)
            logging.info(f" {str(date_time)}: Campagne: {code_compagne} ")
            print(f" {str(date_time)}: Campagne: {code_compagne} ")
            
            ######---------Step 4: Checking if there were changes in the database by controling last id per queried table
            
            #logging
            logging.info(str(date_time) + " -------------------- Checking changes in the database --------------------")
            print(str(date_time) + " -------------------- Checking changes in the database --------------------")
            
            ##getting last generation id per queried table
            #spathes id
            query = f"SELECT TOP 1 spathes_id from generation order by id desc"
            last_spathes_id = pd.read_sql_query(query, dw_cnxn)
            
            #capturing exception if its the first generation and the columns last_spathes_id are empty
            if last_spathes_id.shape[0] == 0:
                
                last_spathes_id = -1
                
            else:
                
                last_spathes_id = last_spathes_id['spathes_id'][0]
            
            #harvest_id
            query = f"SELECT TOP 1 harvest_id from generation order by id desc"
            last_harvest_id = pd.read_sql_query(query, dw_cnxn)
            
            #capturing exception if its the first generation and the columns last_spathes_id are empty
            if last_harvest_id.shape[0] == 0:
                
                last_harvest_id = -1
                
            else:
                
                last_harvest_id = last_harvest_id['harvest_id'][0]
            
            #getting current id per queried table
            ##counted and remaining spathes table
            query = f"SELECT TOP 1 id from Obs_comptage_Arbre ora WHERE (id_element=1 OR id_element=4) AND ID_Ferme in ({turnlist_to_in_statement(idsfermes)}) AND DateCreated BETWEEN {date_debut} AND {date_fin} ORDER BY id DESC"
            spathes_id = pd.read_sql_query(query, prod_cnxn)


            ##harvest table
            query = f"SELECT TOP 1 id from Obs_rendement_Arbre ora WHERE ID_Ferme in ({turnlist_to_in_statement(idsfermes)}) AND DateCreated BETWEEN {date_debut} AND {date_fin} ORDER BY id DESC"
            harvest_id = pd.read_sql_query(query, prod_cnxn)

            print(harvest_id.shape)

            #checking if there are any records in the database for each element
            if harvest_id.shape[0] == 0:

                harvest_id = last_harvest_id
            
            else:

                harvest_id = harvest_id['id'][0]

            #checking if there are any records in the database for each element
            if spathes_id.shape[0] == 0:

                spathes_id = last_spathes_id
            
            else:

                spathes_id = spathes_id['id'][0]


            #comparing last generation id with current id per queried table
            if pipeline_params['force_generation'] == False:
                
                if (spathes_id != last_spathes_id) or (harvest_id != last_harvest_id):
                    
                    #setting control variable if at least on table has changed
                    detected_changes = True
                    #logging
                    logging.info(str(date_time) + ": Detected changes in the database, continuing pipeline")
                    print(str(date_time) + ": Detected changes in the database, continuing pipeline")
                    
                else:
                    
                    detected_changes = False
                    state = "Success"
                    message = "No changes detected in the database, exiting pipeline"
                    
                    #logging
                    logging.info(str(date_time) + ": No changes detected in the database, exiting pipeline")
                    print(str(date_time) + ": No changes detected in the database, exiting pipeline")
        
            else:
                
                detected_changes = True
                #logging
                logging.info(str(date_time) + ": Forced generation, continuing pipeline")
                print(str(date_time) + ": Forced generation, continuing pipeline")

                
            #Controlling if there were some changes in the database to continue pipeline
            if detected_changes:
                
                #####--------PreStep 5: Deleting rows of same campagne to generate up to date rows
                
                try:
                
                    cursor = dw_cnxn.cursor()
                    cursor.execute(f"DELETE FROM detail_predictions WHERE campagne = '{code_compagne}'")
                    dw_cnxn.commit()
                    cursor.close()

                    logging.info(f"{str(date_time)}: Succesfully deleted rows of campagne '{code_compagne}'")
                    print(f"{str(date_time)}: Succesfully deleted rows of campagne '{code_compagne}'")

                except Exception as ex: 
        
                    #logging
                    logging.error(f"{str(date_time)}: Couldnt delete rows of campagne '{code_compagne}'")
                    print(f"{str(date_time)}: Couldnt delete rows of campagne '{code_compagne}")
                    
                    template = "An exception of type {0} occurred. Arguments:\n{1!r}"
                    logging.warning(str(date_time) + template.format(type(ex).__name__, ex.args))
                    print(str(date_time) + template.format(type(ex).__name__, ex.args))

                ######---------Step 5: Querying data (Counted branches data, Remaining branches data, Harvest data, Plot, Farm and Company data)
                    
                #setting queries 
                query_file_branches_counted=pipeline_params['query_file_branches_counted']
                query_file_branches_remaining=pipeline_params['query_file_branches_remaining']
                query_file_harvest=pipeline_params['query_file_harvest']
                    
                #setting queries parameters
                params_countedbranches = {'date_start' : date_debut,
                                            'date_end' : date_fin,
                                            'id_element' : 1,
                                            'id_ferme' : turnlist_to_in_statement(idsfermes)}

                params_remainingbranches= {'date_start' : date_debut,
                                            'date_end' : date_fin,
                                            'id_element' : 4,
                                            'id_ferme' : turnlist_to_in_statement(idsfermes)}

                params_harvest = {'date_start' : date_debut,
                                    'date_end' : date_fin,
                                    'id_ferme' : turnlist_to_in_statement(idsfermes)}

                #call function to query the needed data counted and remaining branches
                df_counted = query_data(prod_cnxn,query_file_branches_counted,params_countedbranches,'counted branches')
                df_remaining = query_data(prod_cnxn,query_file_branches_remaining,params_remainingbranches, 'remaining branches')
                df_harvest = query_data(prod_cnxn,query_file_harvest,params_harvest, 'harvest data')
                    
                ######---------Step 5: Prepare data for modeling using the prepare_data function
                #columns to transform to str
                columns_to_str = ['plot_id','row','id_ferme','id_societe','tree_id']
                
                #columns to include in the agreggation df
                columns_to_agg = ['plot_id','row']
                boxcox_lambda = 0.25
                
                #calling the function to prepare data
                df_counted_prepared,_ = prepare_data(df_counted,columns_to_str,columns_to_agg,True, True, ['counted_branches'],boxcox_lambda)
                df_remaining_prepared,_ = prepare_data(df_remaining,columns_to_str,columns_to_agg,True, True, ['remaining_branches'],boxcox_lambda)
                    
                ######---------Step 6: Predicting using the functions predict

                #setting models
                model_countedbranches=pipeline_params['model_countedbranches']
                model_remainingbranches = pipeline_params['model_remainingbranches']

                #call function to predict yield with counted branches
                if df_counted_prepared is not None:

                    df_counted_predicted = predict_df(df_counted_prepared,model_countedbranches,"Counted branches model",['counted_branches'],True,boxcox_lambda)
                    df_counted_predicted.reset_index(inplace=True)
                
                else:

                    df_counted_predicted = pd.DataFrame(columns=['plot_id','row','counted_branches','predicted_yield'])

                if df_remaining_prepared is not None:

                    df_remaining_predicted = predict_df(df_remaining_prepared,model_remainingbranches,"Remaining branches model",['remaining_branches'],True,boxcox_lambda)
                
                else:

                    df_remaining_predicted = pd.DataFrame(columns=['plot_id','row','remaining_branches','predicted_yield'])  
                    df_counted_predicted.reset_index(inplace=True)                  

                #df_counted.to_excel('df_counted.xlsx')
                #df_remaining.to_excel('df_remaining.xlsx')
                #df_harvest.to_excel('df_harvest.xlsx')
                #df_counted_predicted.to_excel('df_counted_predicted.xlsx')
                #df_remaining_predicted.to_excel('df_remaining_predicted.xlsx')
                    
                ######---------Step 7: Dividing yield on trees based on their spathes count (pondered mean with weights)
                result = divide_yield_ontrees(df_harvest,df_counted,df_remaining, df_counted_predicted,df_remaining_predicted)

                ######---------Step 8: Inserting into the database
                
                #setting generation metadata 
                generated_by = pipeline_params['generated_by']
                state = ''
                message = ''
                
                #inserting generation metadata and getting genereation id
                id = insert_metadata(dw_cnxn, generated_by, spathes_id, harvest_id,code_compagne)
                
                if id is not None:
                    
                    state, message = insert_details(dw_cnxn,id,code_compagne, result) 
                
                else:
                    
                    state = "Failure"
                    message = "Returned metadata is null"         
            
    except Exception as ex: 
        
        #logging
        logging.error(str(date_time) + ': Pipeline unsuccesfully finished')
        print(str(date_time) + ': Pipeline unsuccesfully finished')
        
        template = "An exception of type {0} occurred. Arguments:\n{1!r}"
        logging.warning(str(date_time) + template.format(type(ex).__name__, ex.args))
        print(str(date_time) + template.format(type(ex).__name__, ex.args))
        
        state = "Failure"
        message = f"Pipeline unsuccesfully finished, an exception of type {0} occurred. Arguments:\n{1!r}, {template.format(type(ex).__name__, ex.args)}"
    
    #sending email
    try:
        
        #setting subject
        subject = f'Pipeline generation SAHAM {str(datetime.date.today())}: {state}'
        
        #setting body
        body = f'The prediction pipeline exited with sucess. {message}'
        
        send_email(subject,body)
        
        #logging
        logging.info(str(date_time) + ": Email sent successfully")
        print(str(date_time) + ": Email sent successfully")
    
    except Exception as ex: 

        #logging
        logging.warning(str(date_time) + ": Email not sent")
        print(str(date_time) + ": Email not sent")
        
        template = "An exception of type {0} occurred. Arguments:\n{1!r}"
        logging.warning(str(date_time) + template.format(type(ex).__name__, ex.args))
        print(str(date_time) + template.format(type(ex).__name__, ex.args))
 
    return state

        
            
            




            
            

            

            
            
            
            
    