# importing libraries
import sys
import os
import logging
import datetime
import pickle
import smtplib

# get start time to calculate run time
startTime = datetime.datetime.now()

# functions file where all functions are declared
from functions import prediction_pipeline

# connection file where all database connections are stored
from conn import production_database, warehouse_database

def main():
    
    # get system path and system date
    cwd = os.path.abspath(os.path.join(sys.path[0], os.pardir))

    # Setting the log file
    logfile_name = os.path.join(cwd, "logfile.log")
    logging.basicConfig(filename=logfile_name, level=logging.INFO)
    
    # ---------Importing files, models and queries
    # ---queries
    query_file_branches_counted = open(os.path.join(
        cwd, 'queries', 'get_data_branches_counted')).read()
    query_file_branches_remaining = open(os.path.join(
        cwd, 'queries', 'get_data_branches_remaining')).read()
    query_file_harvest = open(os.path.join(
        cwd, 'queries', 'get_data_harvest')).read()

    # ---models
    model_countedbranches = pickle.load(open(os.path.join(cwd,'models','countedbranches_model.sav'),'rb'))
    model_remainingbranches = pickle.load(open(os.path.join(cwd,'models','remainingbranches_model.sav'),'rb'))

    #default data if automatic generation is true
    automatic_generation = True
    idsociete = None
    idferme = None
    sent_campagne = None
    generated_by = 'Automatic generation' 
    force_generation = False
    
    #filters data if automatic generation is false
    #automatic_generation = False
    #idsociete = 1
    #idferme = 1
    #sent_campagne = 281474976710661
    #generated_by = '1'
    #force_generation = True

    #parameters of the pipeline
    pipeline_params = {'production_database': production_database,
                    'warehouse_database': warehouse_database,
                    'automatic_generation': automatic_generation,
                    'idsociete': idsociete,
                    'idferme': idferme,
                    'sent_campagne': sent_campagne,
                    'generated_by' : generated_by,
                    'force_generation' : force_generation,
                    'query_file_branches_counted': query_file_branches_counted,
                    'query_file_branches_remaining': query_file_branches_remaining,
                    'query_file_harvest': query_file_harvest,
                    'query_file_harvest': query_file_harvest,
                    'model_countedbranches' : model_countedbranches,
                    'model_remainingbranches' : model_remainingbranches
                    }

    # ------------------------Sarting prediction pipeline
    final_state = prediction_pipeline(pipeline_params)    

if __name__ == '__main__':
    main()