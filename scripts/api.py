# importing libraries
from flask import Flask, request
from flask_restful import Resource, Api
import sys
import os
import logging
import datetime
import pickle

#defining flask app
app = Flask(__name__)
api = Api(app)

# get start time to calculate run time
startTime = datetime.datetime.now()

# functions file where all functions are declared
from functions import prediction_pipeline

# connection file where all database connections are stored
from conn import production_database, warehouse_database

# get system path and system date
cwd = os.path.abspath(os.path.join(sys.path[0], os.pardir))

# Setting the log file
logfile_name = os.path.join(cwd, "logfile.log")
logging.basicConfig(filename=logfile_name, level=logging.INFO)

# ---------Importing files, models and queries
# ---queries
query_file_branches_counted = open(os.path.join(
    cwd, 'queries', 'get_data_branches_counted')).read()
query_file_branches_remaining = open(os.path.join(
    cwd, 'queries', 'get_data_branches_remaining')).read()
query_file_harvest = open(os.path.join(
    cwd, 'queries', 'get_data_harvest')).read()

# ---models
model_countedbranches = pickle.load(open(os.path.join(cwd,'models','countedbranches_model.sav'),'rb'))
model_remainingbranches = pickle.load(open(os.path.join(cwd,'models','remainingbranches_model.sav'),'rb'))

#API
class pipeline(Resource):

    def post(self):
        
        print(f"{str(startTime)}: Started pipeline...")
        
        #getting filters data
        data = request.get_json(force=True)
        automatic_generation =  data.get('automatic_generation')
        idsociete =  data.get('idsociete')
        idferme =  data.get('idferme')
        sent_campagne = data.get('sent_campagne')
        generated_by = data.get('generated_by')
        force_generation = data.get('force_generation')
         
        if automatic_generation=='False':
            
            automatic_generation == False
            
        else:
            
            automatic_generation == True
            
        if force_generation=='True':
                
            force_generation == True
            
        else:
            
            force_generation == False

        #parameters of the pipeline
        pipeline_params = {'production_database': production_database,
                        'warehouse_database': warehouse_database,
                        'automatic_generation': automatic_generation,
                        'idsociete': idsociete,
                        'idferme': idferme,
                        'sent_campagne': sent_campagne,
                        'generated_by' : generated_by,
                        'force_generation' : force_generation,
                        'query_file_branches_counted': query_file_branches_counted,
                        'query_file_branches_remaining': query_file_branches_remaining,
                        'query_file_harvest': query_file_harvest,
                        'query_file_harvest': query_file_harvest,
                        'model_countedbranches' : model_countedbranches,
                        'model_remainingbranches' : model_remainingbranches
                        }

        final_state = prediction_pipeline(pipeline_params)  
        
        logging.info(f"{datetime.datetime.now()}: {final_state}")
        print(f"{datetime.datetime.now()}: {final_state}")
        logging.info(f'{datetime.datetime.now()}: Finished pipeline in {datetime.datetime.now() - startTime} seconds, exiting...')
        print(f'{datetime.datetime.now()}: Finished pipeline in {datetime.datetime.now() - startTime} seconds, exiting...')
        
        #return state of pipepline
        return final_state
         
api.add_resource(pipeline, '/')

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')